const express = require('express');
const router = express.Router();
const { createWorkout, getAllWorkouts, getWorkout, deleteWorkout, updateWorkout } = require('../controllers/workoutController');
const requireAuth = require('../middleware/requireAuth');

// middleware require auth for all workout routes
router.use(requireAuth);

// GET all workouts
router.get('/', getAllWorkouts)

// GET a single workouts
router.get('/:id', getWorkout);

// Post a new workout
router.post('/', createWorkout);

// Delete a workout
router.delete('/:id', deleteWorkout);

// Update a workout
router.patch('/:id', updateWorkout);

module.exports = router;