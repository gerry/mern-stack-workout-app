require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');


const workoutRoutes = require('./routes/workouts');
const userRoutes = require('./routes/users');

// express app
const app = express();

//middlewares

// to be able to access the request obj data
app.use(express.json());

app.use((req, res, next) => {
  console.log(req.path, req.method);
  next();
});


// workout routes
app.use('/api/workouts', workoutRoutes);
app.use('/api/user', userRoutes);

// connect to db
mongoose.connect(process.env.MONGO_URI, { useNewUrlParser: true })
  .then(() => {
    console.log('MongoDB Connected...');

    // listen for request
    app.listen(process.env.PORT, () => {
      console.log(`listening on port ${process.env.PORT}`);
    })
    
  })
  .catch(error => console.log(error))
